#!/bin/bash

set -e

cd $(dirname $0)

if [ -z "$PRODUCT_KEY" ] ; then
	PRODUCT_KEY="W269N-WFGWX-YVC9B-4J6C9-T83GX" # Generic KMS client key
fi

if [ ! -e "administrators_authorized_keys" ] ; then
	echo "Missing file 'administrators_authorized_keys', please provide them"
	exit 1
elif [ ! -e "wsl-ubuntu-1804.appx" ] ; then
	echo "Missing file 'wsl-ubuntu-1804.appx'. Please download it from https://aka.ms/wsl-ubuntu-1804 and rename if necessary"
	exit 1
elif [ ! -e "Win10_1809Oct_English_x64.iso" ] ; then
	echo "Missing file 'Win10_1809Oct_English_x64.iso'. Please download it from Win10_1809Oct_English_x64.iso."
	exit 1
fi

if [ "$#" -ne 1 ] ; then
	echo "Missing argument REGISTRATION_TOKEN"
	exit 1
fi

REGISTRATION_TOKEN="$1"

sed Autounattend.xml.in -e "s:@@PRODUCT_KEY@@:$PRODUCT_KEY:" -e "s:@@REGISTRATION_TOKEN@@:$REGISTRATION_TOKEN:" > Autounattend.xml

INCLUDED_FILES="Autounattend.xml LayoutModification.xml administrators_authorized_keys wsl-ubuntu-1804.appx"

for file in $INCLUDED_FILES; do
	if [ ! -e $file ] ; then
		echo "Cannot proceed, missing file $file"
		exit 1
	elif [ -L $file ] ; then
		echo "Cannot proceed, file $file is a symlink"
		exit 1
	fi
done

if [ -e win10-clean.img ] ; then
	echo win10-clean.img already exists
	exit 1
fi


qemu-img create -f qcow2 -o size=64G win10-clean.img

rm -f unattend.{iso,img}
genisoimage -J -o unattend.iso $INCLUDED_FILES

kvm \
	-machine q35 -smp cpus=4,cores=2,threads=2,sockets=1 -m 4096 \
	-rtc base=localtime \
	-vga std -usb -device usb-tablet -soundhw hda -device hda-output -device ahci,id=ahci \
	-drive id=disk,file=win10-clean.img,if=none,cache=none \
	-device ide-drive,drive=disk,bus=ahci.0 \
	-drive id=cdrom,file=Win10_1809Oct_English_x64.iso,if=none,media=cdrom \
	-device ide-drive,drive=cdrom,bus=ahci.1 \
	-drive id=unattend,file=unattend.iso,if=none,media=cdrom \
	-device ide-drive,drive=unattend,bus=ahci.2 \
	-net nic -net user,hostfwd=tcp::7722-:22

rm -f unattend.{iso,img}

chmod a-w win10-clean.img
qemu-img create -f qcow2 -b win10-clean.img win10.img
