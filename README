Unattended Windows 10 installation with SSH server, WSL and a GitLab runner in a VM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Usage:

env PRODUCT_KEY=XXXXX-XXXXX-XXXXX-XXXXX-XXXXX ./install.sh <REGISTRATION_TOKEN>

The product key is optional. Without it, the generic KMS client key will be used.
The registration token can be found at https://gitlab.com/BuildStream/buildstream/settings/ci_cd.

Software required:
* QEMU/KVM
* genisoimage

Required files that are not included:
* Win10_1809Oct_English_x64.iso
  https://www.microsoft.com/en-us/software-download/windows10ISO
* wsl-ubuntu-1804.appx
  https://aka.ms/wsl-ubuntu-1804
* administrators_authorized_keys
  SSH public key to install for passwordless login

**NOTE**:

To improve performance of WSL, you may need to disable various security systems
https://medium.com/@leandrw/speeding-up-wsl-i-o-up-than-5x-fast-saving-a-lot-of-battery-life-cpu-usage-c3537dd03c74
